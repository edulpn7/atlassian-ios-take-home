//
//  PresentationTests.swift
//  atlassian-ios-take-homeTests
//
//  Created by Eduardo Pinto on 3/16/18.
//  Copyright © 2018 Eduardo Pinto. All rights reserved.
//

import XCTest
@testable import atlassian_ios_take_home

import RxSwift
import Nimble

class PresentationTests: XCTestCase {
    
    class StubChatView: ChatViewProtocol {
        var presenter: ChatPresenterProtocol!
        var router: ChatRouterProtocol!
        var loadingView: UIView!
        
        var haveDisplayedError: Bool = false
        var haveDisplayedViewModel: Bool = false
        var haveStartedActivityIndicator: Bool = false
        var haveStoppedActivityIndicator: Bool = false
        
        func startLoading() {
            haveStartedActivityIndicator = true
        }
        
        func stopLoading() {
            haveStoppedActivityIndicator = true
        }
        
        func display(_ error: Error) {
            haveDisplayedError = true
        }
        
        func display(_ viewModel: ParsedChatMessageViewModel) {
            haveDisplayedViewModel = true
        }
    }
    
    struct FakeErrorParseChatMessageUseCase: ParseChatMessageUseCaseProtocol {
        func parse(_ message: String) -> Observable<ParsedChatMessage> {
            return Observable.error(ParseChatMessageUseCaseError.couldNotParseMessage)
        }
    }
    
    struct FakeSuccessParseChatMessageUseCase: ParseChatMessageUseCaseProtocol {
        func parse(_ message: String) -> Observable<ParsedChatMessage> {
            return Observable.just(ParsedChatMessage(inputMessage: "", mentions: [], emoticons: [], urls: []))
        }
    }
    
    var view: StubChatView!
    var presenter: ChatPresenter!
    
    override func setUp() {
        super.setUp()
        view = StubChatView()
        presenter = ChatPresenter()
        presenter.view = view
        view.presenter = presenter
    }
    
    func testSuccessChatPresenterFlow() {
        let useCase = FakeSuccessParseChatMessageUseCase()
        presenter.useCase = useCase
        presenter.parse("")
        
        expect(self.view.haveStartedActivityIndicator).to(be(true))
        expect(self.view.haveStoppedActivityIndicator).to(be(true))
        expect(self.view.haveDisplayedViewModel).to(be(true))
        expect(self.view.haveDisplayedError).to(be(false))
    }
    
    func testErrorChatPresenterFlow() {
        let useCase = FakeErrorParseChatMessageUseCase()
        presenter.useCase = useCase
        presenter.parse("")
        
        expect(self.view.haveStartedActivityIndicator).to(be(true))
        expect(self.view.haveStoppedActivityIndicator).to(be(true))
        expect(self.view.haveDisplayedViewModel).to(be(false))
        expect(self.view.haveDisplayedError).to(be(true))
    }
}
