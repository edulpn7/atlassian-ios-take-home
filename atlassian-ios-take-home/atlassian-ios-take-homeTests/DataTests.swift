//
//  DataTests.swift
//  atlassian-ios-take-homeTests
//
//  Created by Eduardo Pinto on 3/14/18.
//  Copyright © 2018 Eduardo Pinto. All rights reserved.
//

import XCTest
@testable import atlassian_ios_take_home

import RxSwift
import Nimble
import RxNimble

func equalTitle(_ expectedValue: String) -> Predicate<HTMLPage> {
    return Predicate.define("equalTitle <\(stringify(expectedValue))>") { actualExpression, msg in
        guard let actualValue = try actualExpression.evaluate() else {
            return PredicateResult(status: .fail, message: msg)
        }
        return PredicateResult(bool: actualValue.title == expectedValue, message: msg)
    }
}

struct FakeHTMLPageDataSource: HTMLPageDataSourceProtocol {
    static let expectedURLString = "http://test.com"
    static let expectedTitle = "ExpectedTitle"
    func retrieveHTMLPage(from url: URL) -> Observable<HTMLPageEntity> {
        let fakeHTML = "<HTML><HEAD><TITLE>\(FakeHTMLPageDataSource.expectedTitle)</TITLE></HEAD><BODY BGCOLOR=\"FFFFFF\"><HR>HR<H1>This is a Header</H1><H2>This is a Medium Header</H2><P> This is a new paragraph!<P> <B>This is a new paragraph!</B><BR> <B><I>This is a new sentence without a paragraph break, in bold italics.</I></B><HR></BODY></HTML>"
        
        return Observable.just(HTMLPageEntity(url: FakeHTMLPageDataSource.expectedURLString, content: fakeHTML.data(using: .utf8)!))
    }
}

class DataTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testHTMLScrapping() {
        let repository = HTMLPageRepository(dataSource: FakeHTMLPageDataSource())
        expect(repository.retrieveHTMLPage(from: URL(string: FakeHTMLPageDataSource.expectedURLString)!)).first.to(equalTitle(FakeHTMLPageDataSource.expectedTitle))
    }
}
