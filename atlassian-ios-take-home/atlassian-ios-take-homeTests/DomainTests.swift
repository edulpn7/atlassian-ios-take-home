//
//  DomainTests.swift
//  atlassian-ios-take-homeTests
//
//  Created by Eduardo Pinto on 3/14/18.
//  Copyright © 2018 Eduardo Pinto. All rights reserved.
//

import XCTest
@testable import atlassian_ios_take_home

import RxSwift
import Nimble
import RxNimble

func haveURLCount(_ expectedValue: Int) -> Predicate<ParsedChatMessage> {
    return Predicate.define("haveURLCount <\(stringify(expectedValue))>") { actualExpression, msg in
        guard let actualValue = try actualExpression.evaluate() else {
            return PredicateResult(status: .fail, message: msg)
        }
        return PredicateResult(bool: actualValue.urls.count == expectedValue, message: msg)
    }
}

func haveMentionCount(_ expectedValue: Int) -> Predicate<ParsedChatMessage> {
    return Predicate.define("haveMentionCount <\(stringify(expectedValue))>") { actualExpression, msg in
        guard let actualValue = try actualExpression.evaluate() else {
            return PredicateResult(status: .fail, message: msg)
        }
        return PredicateResult(bool: actualValue.mentions.count == expectedValue, message: msg)
    }
}

func haveEmoticonCount(_ expectedValue: Int) -> Predicate<ParsedChatMessage> {
    return Predicate.define("haveEmoticonCount <\(stringify(expectedValue))>") { actualExpression, msg in
        guard let actualValue = try actualExpression.evaluate() else {
            return PredicateResult(status: .fail, message: msg)
        }
        return PredicateResult(bool: actualValue.emoticons.count == expectedValue, message: msg)
    }
}

class DomainTests: XCTestCase {
    
    func testMessageParsing() {
        let repository = HTMLPageRepository(dataSource: FakeHTMLPageDataSource())
        let useCase = ParseChatMessageUseCase(repository: repository)
        let message = "http://test.com https://test.com www.test.com test.com @test1 @test2, @test$#%& @$%& @ (test) (test1) (test ) (test$%&*#) () (#$%&*) (longerthanfifteencharactersemoticon)"
        let parsedMessage = useCase.parse(message)
        
        expect(parsedMessage).first.to(haveURLCount(4))
        expect(parsedMessage).first.to(haveMentionCount(3))
        expect(parsedMessage).first.to(haveEmoticonCount(2))
    }
}
