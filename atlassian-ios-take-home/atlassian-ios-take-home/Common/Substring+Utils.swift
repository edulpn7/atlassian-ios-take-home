//
//  Substring+Utils.swift
//  atlassian-ios-take-home
//
//  Created by Eduardo Pinto on 3/12/18.
//  Copyright © 2018 Eduardo Pinto. All rights reserved.
//

import Foundation

extension Substring {
    var isAlphanumeric: Bool {
        if self.rangeOfCharacter(from: CharacterSet.alphanumerics.inverted) != nil {
            return false
        } else {
            return true
        }
    }
    
    var prefixUntilNonAlphanumericCharacter: Substring {
        return self.prefix(while: { character -> Bool in
            guard let scalar = character.unicodeScalars.first else {
                return false
            }
            if CharacterSet.alphanumerics.inverted.contains(scalar) {
                return false
            } else {
                return true
            }
        })
    }
}
