//
//  AtlassianCustomNavigationControllerStyle.swift
//  atlassian-ios-take-home
//
//  Created by Eduardo Pinto on 3/16/18.
//  Copyright © 2018 Eduardo Pinto. All rights reserved.
//

import UIKit

struct AtlassianCustomNavigationControllerStyle: CustomNavigationControllerStyle {
    var isBarHidden: Bool = false
    var statusBarStyle: UIStatusBarStyle = .lightContent
    var barTintColor: UIColor? = UIColor(red: 20.0/255.0, green: 58.0/255.0, blue: 168.0/255.0, alpha: 1.0)
    var tintColor: UIColor = UIColor(red: 220.0/255.0, green: 234.0/255.0, blue: 255.0/255.0, alpha: 1.0)
    var shadowImage: UIImage? = nil
    var backgroundImage: UIImage? = nil
    var isTranslucent: Bool = false
    var titleTextAttributes: [NSAttributedStringKey : Any]? = [.foregroundColor: UIColor(red: 220.0/255.0, green: 234.0/255.0, blue: 255.0/255.0, alpha: 1.0)]
    var shouldAnimateStyleChanges: Bool = true
}
