//
//  ParseChatMessageUseCase.swift
//  atlassian-ios-take-home
//
//  Created by Eduardo Pinto on 3/12/18.
//  Copyright © 2018 Eduardo Pinto. All rights reserved.
//

import Foundation
import RxSwift

protocol ParseChatMessageUseCaseProtocol {
    func parse(_ message: String) -> Observable<ParsedChatMessage>
}

struct ParseChatMessageUseCase: ParseChatMessageUseCaseProtocol {
    let repository: HTMLPageRepositoryProtocol
    let disposeBag = DisposeBag()
    
    func parse(_ message: String) -> Observable<ParsedChatMessage> {
        let messageComponents = message.components(separatedBy: CharacterSet.whitespacesAndNewlines)
        var mentions: [String] = []
        var emoticons: [String] = []
        var urls: [URL] = []
        
        let urlDetector = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
        
        messageComponents.forEach { substring in
            if let urlMatch = urlDetector.firstMatch(in: substring, options: [], range: NSRange(location: 0, length: substring.count)), urlMatch.resultType == .link, let url = urlMatch.url {
                urls.append(url)
            } else {
                if substring.hasPrefix("@") {
                    let possibleMention = substring.dropFirst().prefixUntilNonAlphanumericCharacter
                    
                    if possibleMention.count > 0 {
                        mentions.append(String(possibleMention))
                    }
                    
                } else if substring.hasPrefix("("), substring.hasSuffix(")") {
                    let possibleEmoticon = substring.dropFirst().dropLast()
                    
                    if possibleEmoticon.isAlphanumeric, possibleEmoticon.count <= 15, possibleEmoticon.count > 0 {
                        emoticons.append(String(possibleEmoticon))
                    }
                }
            }
        }
        
        if urls.count > 0 {
            return Observable.zip(urls.map { url -> Observable<HTMLPage> in
                return repository.retrieveHTMLPage(from: url)
            }).flatMap { htmlPages -> Observable<ParsedChatMessage> in
                return Observable.just(ParsedChatMessage(inputMessage: message, mentions: mentions, emoticons: emoticons, urls: htmlPages))
            }
        } else {
            return Observable.just(ParsedChatMessage(inputMessage: message, mentions: mentions, emoticons: emoticons, urls: []))
        }
    }
}
