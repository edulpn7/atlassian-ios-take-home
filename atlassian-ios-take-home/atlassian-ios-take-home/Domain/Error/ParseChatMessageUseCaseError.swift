//
//  ParseChatMessageUseCaseError.swift
//  atlassian-ios-take-home
//
//  Created by Eduardo Pinto on 3/14/18.
//  Copyright © 2018 Eduardo Pinto. All rights reserved.
//

import Foundation

enum ParseChatMessageUseCaseError: Error {
    case couldNotParseMessage
}
