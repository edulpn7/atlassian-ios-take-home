//
//  ParsedChatMessage.swift
//  atlassian-ios-take-home
//
//  Created by Eduardo Pinto on 3/12/18.
//  Copyright © 2018 Eduardo Pinto. All rights reserved.
//

import Foundation

struct ParsedChatMessage {
    let inputMessage: String
    let mentions: [String]
    let emoticons: [String]
    let urls: [HTMLPage]
}
