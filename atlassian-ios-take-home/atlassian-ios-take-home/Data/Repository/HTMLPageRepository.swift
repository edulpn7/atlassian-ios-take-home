//
//  PageContentRepository.swift
//  atlassian-ios-take-home
//
//  Created by Eduardo Pinto on 3/12/18.
//  Copyright © 2018 Eduardo Pinto. All rights reserved.
//

import Foundation
import RxSwift

struct HTMLPageRepository: HTMLPageRepositoryProtocol {
    let dataSource: HTMLPageDataSourceProtocol
    
    func retrieveHTMLPage(from url: URL) -> Observable<HTMLPage> {
        return dataSource.retrieveHTMLPage(from: url).mapHTMLPage()
    }
}
