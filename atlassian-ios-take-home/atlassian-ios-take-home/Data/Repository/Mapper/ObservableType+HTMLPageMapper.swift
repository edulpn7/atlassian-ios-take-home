//
//  ObservableType+HTMLPageMapper.swift
//  atlassian-ios-take-home
//
//  Created by Eduardo Pinto on 3/12/18.
//  Copyright © 2018 Eduardo Pinto. All rights reserved.
//

import Foundation
import RxSwift
import Kanna

extension ObservableType where E == HTMLPageEntity {
    func mapHTMLPage() -> Observable<HTMLPage> {
        return flatMap { (entity) -> Observable<HTMLPage> in
            var title = ""
            if let document = try? Kanna.HTML(html: entity.content, encoding: .utf8) {
                title = document.title ?? ""
            }
            return Observable.just(HTMLPage(url: entity.url, title: title))
        }
    }
}
