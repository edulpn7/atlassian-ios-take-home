//
//  ResponseError.swift
//  atlassian-ios-take-home
//
//  Created by Eduardo Pinto on 3/12/18.
//  Copyright © 2018 Eduardo Pinto. All rights reserved.
//

import Foundation

enum ResponseError: Error {
    case couldNotMapToString
}
