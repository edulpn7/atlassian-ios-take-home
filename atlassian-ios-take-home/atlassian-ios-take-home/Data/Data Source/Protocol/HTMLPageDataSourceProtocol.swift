//
//  HTMLPageDataSourceProtocol.swift
//  atlassian-ios-take-home
//
//  Created by Eduardo Pinto on 3/9/18.
//  Copyright © 2018 Eduardo Pinto. All rights reserved.
//

import Foundation
import RxSwift

protocol HTMLPageDataSourceProtocol {
    func retrieveHTMLPage(from url: URL) -> Observable<HTMLPageEntity>
}
