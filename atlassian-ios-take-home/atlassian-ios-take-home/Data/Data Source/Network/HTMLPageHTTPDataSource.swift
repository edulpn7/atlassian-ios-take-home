//
//  HTMLPageHTTPDataSource.swift
//  atlassian-ios-take-home
//
//  Created by Eduardo Pinto on 3/9/18.
//  Copyright © 2018 Eduardo Pinto. All rights reserved.
//

import Foundation
import RxSwift
import Moya

struct HTMLPageHTTPDataSource: HTMLPageDataSourceProtocol {
    let provider = MoyaProvider<HTML>()
    
    func retrieveHTMLPage(from url: URL) -> Observable<HTMLPageEntity> {
        return provider.rx.request(HTML.HTMLContent(url: url))
            .debug()
            .asObservable()
            .filterSuccessfulStatusCodes()
            .mapHTMLPageEntity()
    }
}
