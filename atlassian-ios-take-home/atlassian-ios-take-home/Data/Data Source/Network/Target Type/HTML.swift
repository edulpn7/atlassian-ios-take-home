//
//  HTML.swift
//  atlassian-ios-take-home
//
//  Created by Eduardo Pinto on 3/9/18.
//  Copyright © 2018 Eduardo Pinto. All rights reserved.
//

import Foundation
import Moya

enum HTML {
    case HTMLContent(url: URL)
}

extension HTML: TargetType {
    var baseURL: URL {
        switch self {
        case .HTMLContent(let url):
            return url
        }
    }
    
    var headers: [String : String]? {
        return nil
    }
    
    var method: Moya.Method {
        switch self {
        case .HTMLContent:
            return .get
        }
    }
    
    var path: String {
        switch self {
        case .HTMLContent:
            return ""
        }
    }
    
    var sampleData: Data {
        return Data()
    }
    
    var task: Task {
        switch self {
        case .HTMLContent:
            return .requestPlain
        }
    }
}
