//
//  ObservableType+HTMLContentEntityMapper.swift
//  atlassian-ios-take-home
//
//  Created by Eduardo Pinto on 3/12/18.
//  Copyright © 2018 Eduardo Pinto. All rights reserved.
//

import Foundation
import RxSwift
import Moya

extension ObservableType where E == Response {
    func mapHTMLPageEntity() -> Observable<HTMLPageEntity> {
        return flatMap { (response) -> Observable<HTMLPageEntity> in
            return Observable.just(HTMLPageEntity(url: response.request?.url?.absoluteString ?? "", content: response.data))
        }
    }
}
