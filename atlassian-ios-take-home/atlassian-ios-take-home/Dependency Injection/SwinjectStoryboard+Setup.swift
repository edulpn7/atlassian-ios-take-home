//
//  SwinjectStoryboard+Setup.swift
//  atlassian-ios-take-home
//
//  Created by Eduardo Pinto on 3/9/18.
//  Copyright © 2018 Eduardo Pinto. All rights reserved.
//

import Foundation
import Swinject
import SwinjectStoryboard

extension SwinjectStoryboard {
    @objc class func setup() {
        Container.loggingFunction = nil
        SceneDependencyInjection.registerScenes(with: defaultContainer)
        UseCaseDependencyInjection.registerUseCases(with: defaultContainer)
        RepositoryDependencyInjection.registerRepositories(with: defaultContainer)
        DataSourceDependencyInjection.registerDataSources(with: defaultContainer)
    }
}
