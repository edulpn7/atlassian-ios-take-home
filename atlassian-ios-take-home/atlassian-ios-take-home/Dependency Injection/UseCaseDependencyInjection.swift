//
//  UseCaseDependencyInjection.swift
//  atlassian-ios-take-home
//
//  Created by Eduardo Pinto on 3/9/18.
//  Copyright © 2018 Eduardo Pinto. All rights reserved.
//

import Foundation
import Swinject

struct UseCaseDependencyInjection {
    static func registerUseCases(with container: Container) {
        container.register(ParseChatMessageUseCase.self) { resolver -> ParseChatMessageUseCase in
            return ParseChatMessageUseCase(repository: resolver.resolve(HTMLPageRepository.self)!)
        }
    }
}
