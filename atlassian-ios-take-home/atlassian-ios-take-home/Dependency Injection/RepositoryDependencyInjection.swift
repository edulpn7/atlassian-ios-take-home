//
//  RepositoryDependencyInjection.swift
//  atlassian-ios-take-home
//
//  Created by Eduardo Pinto on 3/9/18.
//  Copyright © 2018 Eduardo Pinto. All rights reserved.
//

import Foundation
import Swinject

struct RepositoryDependencyInjection {
    static func registerRepositories(with container: Container) {
        container.register(HTMLPageRepository.self) { resolver -> HTMLPageRepository in
            HTMLPageRepository(dataSource: resolver.resolve(HTMLPageHTTPDataSource.self)!)
        }
    }
}
