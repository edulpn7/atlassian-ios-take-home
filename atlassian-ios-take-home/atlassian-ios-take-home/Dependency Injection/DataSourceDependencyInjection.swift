//
//  DataSourceDependencyInjection.swift
//  atlassian-ios-take-home
//
//  Created by Eduardo Pinto on 3/9/18.
//  Copyright © 2018 Eduardo Pinto. All rights reserved.
//

import Foundation
import Swinject

struct DataSourceDependencyInjection {
    static func registerDataSources(with container: Container) {
        container.register(HTMLPageHTTPDataSource.self) { resolver -> HTMLPageHTTPDataSource in
            return HTMLPageHTTPDataSource()
        }
    }
}
