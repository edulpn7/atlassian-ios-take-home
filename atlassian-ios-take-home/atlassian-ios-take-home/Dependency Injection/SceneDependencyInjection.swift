//
//  SceneDependencyInjection.swift
//  atlassian-ios-take-home
//
//  Created by Eduardo Pinto on 3/9/18.
//  Copyright © 2018 Eduardo Pinto. All rights reserved.
//

import Foundation
import Swinject

struct SceneDependencyInjection {
    static func registerScenes(with container: Container) {
        ChatConfigurator.configureChatScene(with: container)
    }
}
