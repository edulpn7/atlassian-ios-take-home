//
//  ChatViewController.swift
//  atlassian-ios-take-home
//
//  Created by Eduardo Pinto on 3/16/18.
//  Copyright (c) 2018 Eduardo Pinto. All rights reserved.
//

import UIKit

protocol ChatViewProtocol: class, Loadable, ErrorDisplayer {
    var presenter: ChatPresenterProtocol! {get set}
    var router: ChatRouterProtocol! {get set}
    
    func display(_ viewModel: ParsedChatMessageViewModel)
}

class ChatViewController: UIViewController {
    var presenter: ChatPresenterProtocol!
    var router: ChatRouterProtocol!
    @IBOutlet var loadingView: UIView!
    
    @IBOutlet weak var contentTableView: UITableView!
    @IBOutlet weak var messageTextField: UITextField!
    var viewModels: [ParsedChatMessageViewModel] = []
    
    // MARK:- View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = R.string.localized.chatTitle()
        configureTableView()
    }
    
    func configureTableView() {
        contentTableView.delegate = self
        contentTableView.dataSource = self
        contentTableView.register(R.nib.chatMessageTableViewCell)
        contentTableView.rowHeight = UITableViewAutomaticDimension
    }
    
    @IBAction func sendButtonTouched(_ sender: Any) {
        if let message = messageTextField.text {
            messageTextField.text = ""
            presenter.parse(message)
        }
    }
}

extension ChatViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModels.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: R.nib.chatMessageTableViewCell, for: indexPath) else {
            return UITableViewCell()
        }
        let viewModel = viewModels[indexPath.row]
        cell.configure(with: viewModel.message)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(viewModels[indexPath.row].json)
    }
}

// MARK:- Display logic
extension ChatViewController: ChatViewProtocol {
    func display(_ viewModel: ParsedChatMessageViewModel) {
        viewModels.append(viewModel)
        contentTableView.reloadData()
    }
}

extension ChatViewController: CustomNavigationControllerStylable {
    var customNavigationControllerStyle: CustomNavigationControllerStyle? {
        return AtlassianCustomNavigationControllerStyle()
    }
}
