//
//  ChatRouter.swift
//  atlassian-ios-take-home
//
//  Created by Eduardo Pinto on 3/16/18.
//  Copyright (c) 2018 Eduardo Pinto. All rights reserved.
//

import Foundation
import UIKit

protocol ChatRouterProtocol: class {
    weak var view: ChatViewProtocol! {get set}
}

class ChatRouter: ChatRouterProtocol {
    weak var view: ChatViewProtocol!
}
