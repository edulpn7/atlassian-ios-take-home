//
//  ChatConfigurator.swift
//  atlassian-ios-take-home
//
//  Created by Eduardo Pinto on 3/16/18.
//  Copyright (c) 2018 Eduardo Pinto. All rights reserved.
//

import UIKit
import Swinject

struct ChatConfigurator {
    static func configureChatScene(with container: Container) {
        container.storyboardInitCompleted(ChatViewController.self) { (resolver, viewController) in
            let useCase = resolver.resolve(ParseChatMessageUseCase.self)!
            let presenter = ChatPresenter()
            let router = ChatRouter()
            presenter.view = viewController
            presenter.useCase = useCase
            router.view = viewController
            viewController.presenter = presenter
            viewController.router = router
        }
    }
}
