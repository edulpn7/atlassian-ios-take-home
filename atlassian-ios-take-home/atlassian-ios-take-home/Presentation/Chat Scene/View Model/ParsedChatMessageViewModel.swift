//
//  ParsedChatMessageViewModel.swift
//  atlassian-ios-take-home
//
//  Created by Eduardo Pinto on 3/16/18.
//  Copyright © 2018 Eduardo Pinto. All rights reserved.
//

import Foundation

struct ParsedChatMessageViewModel {
    let message: String
    let json: String
    
    init(mapping model: ParsedChatMessage) throws {
        message = model.inputMessage
        let encoder = JSONEncoder()
        encoder.outputFormatting = .prettyPrinted
        guard let jsonData = try? encoder.encode(model), let jsonString = String(data: jsonData, encoding: .utf8) else {
            throw ParsedChatMessageViewModelError.couldNotMapModel
        }
        json = jsonString
    }
}

extension ParsedChatMessage: Encodable {
    enum ParsedChatMessageCodingKeys: String, CodingKey {
        case mentions
        case emoticons
        case urls = "links"
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: ParsedChatMessageCodingKeys.self)
        if mentions.count > 0 {
            try container.encode(mentions, forKey: .mentions)
        }
        if emoticons.count > 0 {
            try container.encode(emoticons, forKey: .emoticons)
        }
        if urls.count > 0 {
            try container.encode(urls, forKey: .urls)
        }
    }
}

extension HTMLPage: Encodable {
    enum HTMLPageCodingKeys: String, CodingKey {
        case url
        case title
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: HTMLPageCodingKeys.self)
        try container.encode(url, forKey: .url)
        try container.encode(title, forKey: .title)
    }
}
