//
//  ChatMessageTableViewCell.swift
//  atlassian-ios-take-home
//
//  Created by Eduardo Pinto on 3/16/18.
//  Copyright © 2018 Eduardo Pinto. All rights reserved.
//

import UIKit

class ChatMessageTableViewCell: UITableViewCell {
    
    @IBOutlet weak var messageLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        return
    }
    
    func configure(with chatMessage: String) {
        messageLabel.text = chatMessage
    }
}
