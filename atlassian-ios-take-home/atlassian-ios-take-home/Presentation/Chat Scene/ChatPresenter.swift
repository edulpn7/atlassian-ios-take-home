//
//  ChatPresenter.swift
//  atlassian-ios-take-home
//
//  Created by Eduardo Pinto on 3/16/18.
//  Copyright (c) 2018 Eduardo Pinto. All rights reserved.
//

import Foundation
import RxSwift

protocol ChatPresenterProtocol: class {
    weak var view: ChatViewProtocol! {get set}
    var useCase: ParseChatMessageUseCaseProtocol! {get set}
    
    func parse(_ message: String)
}

class ChatPresenter: ChatPresenterProtocol {
    weak var view: ChatViewProtocol!
    var useCase: ParseChatMessageUseCaseProtocol!
    let disposeBag = DisposeBag()
    
    func parse(_ message: String) {
        view.startLoading()
        useCase.parse(message).subscribe(onNext: { [weak self] parsedMessage in
            guard let weakSelf = self else {
                return
            }
            weakSelf.view.stopLoading()
            do {
                let viewModel = try ParsedChatMessageViewModel(mapping: parsedMessage)
                weakSelf.view.display(viewModel)
            } catch let error {
                weakSelf.view.display(error)
            }
            
        }, onError: { [weak self] error in
            guard let weakSelf = self else {
                return
            }
            weakSelf.view.stopLoading()
            weakSelf.view.display(error)

        }).disposed(by: disposeBag)
    }
}
